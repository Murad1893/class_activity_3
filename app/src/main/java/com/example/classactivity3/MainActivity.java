package com.example.classactivity3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  {

    private static String TAG = "MainActivity";

    // displaying all toasts on the complete life cycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, "This is a create toast", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "Activity create called");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "This is a start toast", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "Activity start called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "This is a resume toast", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "Activity resume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "This is a pause toast", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "Activity pause called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "This is a stop toast", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "Activity stop called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "This is a destroy toast", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "Activity destroyed called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(this, "This is a restart toast", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "Activity restart called");
    }
}